package com.ltp3.relatorio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ltp3.persistencia.BancoController;

public class MainActivity extends Activity {

    // UI references.
    private AutoCompleteTextView txtLogin;
    private EditText txtSenha;
    String login;
    String senha;

    private void intentInserirMedico(String login, String senha){
        Intent intent = new Intent (this, InserirMedicoActivity.class);
        intent.putExtra("LOGIN", login);
        intent.putExtra("SENHA", senha);
        this.startActivity(intent);
    }

    private void intentLoginMedico(long id){
        Intent intent = new Intent (this, NavegacaoActivity.class);
        intent.putExtra("id", id);
        this.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCadastro = (Button)findViewById(R.id.btnCadastro);
        Button btnLogin = (Button)findViewById(R.id.btnLogin);

        btnCadastro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txtLogin = (AutoCompleteTextView)findViewById(R.id.txtLogin);
                login = txtLogin.getText().toString();
                txtSenha = (EditText)findViewById(R.id.txtSenha);
                senha = txtSenha.getText().toString();
                intentInserirMedico(login, senha);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BancoController crud = new BancoController(getBaseContext());
                txtLogin = (AutoCompleteTextView)findViewById(R.id.txtLogin);
                login = txtLogin.getText().toString();
                senha = ((EditText)findViewById(R.id.txtSenha)).getText().toString();
                int id = crud.loginMedico(login,senha);
                if (id!=0) {
                    intentLoginMedico(id);
                } else {
                    Toast.makeText(getApplicationContext(), "Credenciais inválidas", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}


























