package com.ltp3.relatorio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ltp3.persistencia.BancoController;

public class InserirMedicoActivity extends Activity {

    EditText login;
    EditText senha;

    private void intentLoginMedico(long id){
        Intent intent = new Intent (this, NavegacaoActivity.class);
        intent.putExtra("ID", id);
        this.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inserir_medico);
        if(getIntent().hasExtra("LOGIN")){
            login = (EditText)findViewById(R.id.txtCreateLogin);
            Bundle extras = getIntent().getExtras();
            login.setText(extras.getString("LOGIN"));
        }
        if(getIntent().hasExtra("SENHA")){
            senha = (EditText)findViewById((R.id.txtCreateSenha));
            Bundle extras = getIntent().getExtras();
            senha.setText(extras.getString("SENHA"));
        }


        Button btnSalvar = (Button)findViewById(R.id.btnSalvar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Medico medico;
                BancoController crud = new BancoController(getBaseContext());
                login = (EditText)findViewById(R.id.txtCreateLogin);
                senha = (EditText)findViewById((R.id.txtCreateSenha));
                EditText nome = (EditText)findViewById(R.id.txtCreateNome);

                String nomeString = nome.getText().toString();
                String senhaString = senha.getText().toString();
                String loginString = login.getText().toString();
                String resultado;

                if (!nomeString.isEmpty() || !senhaString.isEmpty() || !loginString.isEmpty()) {
                    medico = new Medico(nomeString, senhaString, loginString);
                    long id = crud.inserirMedico(medico);
                    if (id!=-1){
                        System.out.println(id);
                        resultado = "Médico cadastrado com sucesso!";
                        intentLoginMedico(id);
                    } else {
                        resultado = "Infelizmente não foi possível cadastrar o médico.";
                    }
                } else {
                    resultado = "Por favor, preencha todos os campos.";
                }
                Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
            }
        });
    }
}