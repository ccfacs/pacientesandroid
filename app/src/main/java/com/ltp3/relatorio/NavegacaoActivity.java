package com.ltp3.relatorio;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.ltp3.persistencia.BancoController;
import com.ltp3.persistencia.DatabaseHelper;

public class NavegacaoActivity extends AppCompatActivity {


    long id = 0;
    RadioButton radioNome;
    RadioButton radioData;
    Button btnSair;
    Button btnPesquisa;
    Button btnCadastrarPaciente;
    EditText txtNome;
    EditText txtData;
    private ListView lista;


    private void intentLogin(){
        Intent intent = new Intent (this, MainActivity.class);
        this.startActivity(intent);
    }

    private void intentCadastroPaciente(long id){
        Intent intent = new Intent (this, CadastroPacienteActivity.class);
        intent.putExtra("ID", id);
        this.startActivity(intent);
    }

    private void intentNav(){
        Intent intent = new Intent (this, NavegacaoActivity.class);
        this.startActivity(intent);
    }

    private void displayListView(){
        Cursor cursor;
        BancoController crud = new BancoController(getBaseContext());

        EditText txt;
        if (radioNome.isChecked()) {
            txt = (EditText)findViewById(R.id.txtBuscaNome);
            cursor = crud.listarPacientes(id, txt.getText().toString(), null);
        } else if (radioData.isChecked()) {
            txt = (EditText)findViewById(R.id.txtBuscaData);
            cursor = crud.listarPacientes(id, null, txt.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), "Por favor insira a informação a ser pesquisada!", Toast.LENGTH_LONG).show();
            return;
        }

        if (cursor.getCount()==0) {
            Toast.makeText(getApplicationContext(), "Não foram encontrados resultados.", Toast.LENGTH_LONG).show();
            return;
        }


        for (String i : cursor.getColumnNames()) {
            System.out.println(i);
        }

        int[] to = new int[]{
                R.id.nomePaciente,
                R.id.descricaoPaciente,
                R.id.dataPaciente,
                R.id.doencaPaciente,
                R.id.medicacaoPaciente,
                R.id.custoPaciente
        };
        String[] columns = new String[]{
                DatabaseHelper.NOME,
                DatabaseHelper.DESCRICAO,
                DatabaseHelper.CHEGADA,
                DatabaseHelper.DOENCA,
                DatabaseHelper.MEDICACAO,
                DatabaseHelper.CUSTO
        };

        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(
                this, R.layout.activity_paciente_layout,
                cursor,
                columns,
                to,
                0);
        setContentView(R.layout.activity_show);
        ListView listView = (ListView) findViewById(R.id.listPacientes);
        Button b = (Button) findViewById(R.id.btnVoltar);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentNav();
            }
        });
        listView.setAdapter(dataAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegacao);
        if(getIntent().hasExtra("ID")){
            Bundle extras = getIntent().getExtras();
            id = extras.getLong("ID");
        }

        radioNome = (RadioButton)findViewById(R.id.radioNome);
        radioData = (RadioButton)findViewById(R.id.radioData);
        btnSair = (Button)findViewById(R.id.btnSair);
        btnPesquisa = (Button)findViewById(R.id.btnPesquisa);
        btnCadastrarPaciente = (Button)findViewById(R.id.btnCadastrarPaciente);

        btnCadastrarPaciente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intentCadastroPaciente(id);
            }
        });

        btnSair.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intentLogin();
            }
        });



        btnPesquisa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                displayListView();
            }
        });

        radioNome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txtNome = (EditText) findViewById(R.id.txtBuscaNome);
                txtData = (EditText) findViewById(R.id.txtBuscaData);
                radioNome = (RadioButton)findViewById(R.id.radioNome);
                if (radioNome.isChecked()) {
                    txtNome.setVisibility(View.VISIBLE);
                    txtData.setVisibility(View.INVISIBLE);
                    radioData.setChecked(false);
                }
            }
        });

        radioData.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txtNome = (EditText) findViewById(R.id.txtBuscaNome);
                txtData = (EditText) findViewById(R.id.txtBuscaData);
                radioData = (RadioButton)findViewById(R.id.radioData);
                if (radioData.isChecked()) {
                    txtNome.setVisibility(View.INVISIBLE);
                    txtData.setVisibility(View.VISIBLE);
                    radioNome.setChecked(false);
                }
            }
        });
    }
}
