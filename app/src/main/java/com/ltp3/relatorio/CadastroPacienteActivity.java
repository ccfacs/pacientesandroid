package com.ltp3.relatorio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ltp3.persistencia.BancoController;

import java.math.BigDecimal;

public class CadastroPacienteActivity extends AppCompatActivity {
    long id;
    Button btnCadastrarPaciente, btnVoltar;
    EditText nome, descricao, doenca, data, medicacao, custo;

    private void intentNav(){
        Intent intent = new Intent (this, NavegacaoActivity.class);
        this.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_paciente);
        if(getIntent().hasExtra("ID")){
            Bundle extras = getIntent().getExtras();
            id = extras.getLong("ID");
        }

        btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentNav();
            }
        });

        btnCadastrarPaciente = (Button)findViewById(R.id.btnCadastrarPaciente);

        btnCadastrarPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EntradaPaciente entrada;

                BancoController crud = new BancoController(getBaseContext());
                nome = (EditText)findViewById(R.id.txtPacienteNome);
                descricao = (EditText)findViewById(R.id.txtPacienteDescricao);
                doenca = (EditText)findViewById(R.id.txtPacienteDoenca);
                data = (EditText)findViewById(R.id.txtPacienteData);
                medicacao = (EditText)findViewById(R.id.txtPacienteMedicacao);
                custo = (EditText)findViewById(R.id.txtPacienteCusto);

                String nomeString = nome.getText().toString();
                String descricaoString = descricao.getText().toString();
                String doencaString = doenca.getText().toString();
                String dataString = data.getText().toString();
                String medicacaoString = medicacao.getText().toString();
                String custoString = custo.getText().toString();
                String resultado;

                if (!nomeString.isEmpty() || !descricaoString.isEmpty() || !doencaString.isEmpty() || !dataString.isEmpty() || !medicacaoString.isEmpty()) {
                    Paciente paciente = new Paciente(nomeString, descricaoString);
                    Medico medico = new Medico(id);
                    entrada = new EntradaPaciente(paciente, medico, doencaString, dataString, medicacaoString, new BigDecimal(custoString));
                    resultado = crud.inserirEntrada(entrada);
                } else {
                    resultado = "Por favor, preencha todos os campos.";
                }
                Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
            }
        });



    }
}
