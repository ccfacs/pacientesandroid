package com.ltp3.relatorio;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by aluno on 09/04/2016.
 */
public class Paciente {
    private long id;
    private String nome;
    private String descricao;

    public Paciente(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
