package com.ltp3.relatorio;

import java.math.BigDecimal;
import java.util.Date;

public class EntradaPaciente {
    private long id;
    private Paciente paciente;
    private Medico medico;
    private String doenca;
    private String dataChegada;
    private String medicacao;
    private BigDecimal custo;

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public String getDoenca() {
        return doenca;
    }

    public void setDoenca(String doenca) {
        this.doenca = doenca;
    }

    public String getDataChegada() {
        return dataChegada;
    }

    public void setDataChegada(String dataChegada) {
        this.dataChegada = dataChegada;
    }

    public String getMedicacao() {
        return medicacao;
    }

    public void setMedicacao(String medicacao) {
        this.medicacao = medicacao;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }


    public EntradaPaciente(Paciente paciente, Medico medico, String doenca, String dataChegada, String medicacao, BigDecimal custo) {
        this.paciente = paciente;
        this.medico = medico;
        this.doenca = doenca;
        this.dataChegada = dataChegada;
        this.medicacao = medicacao;
        this.custo = custo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
