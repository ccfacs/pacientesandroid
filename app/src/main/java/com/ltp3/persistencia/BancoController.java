package com.ltp3.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import com.ltp3.relatorio.EntradaPaciente;
import com.ltp3.relatorio.Medico;
import com.ltp3.relatorio.Paciente;
import com.ltp3.relatorio.R;

import java.util.ArrayList;


public class BancoController {

    private SQLiteDatabase db;
    private DatabaseHelper banco;

    public BancoController(Context context) {
        banco = new DatabaseHelper(context);
    }

    public long inserirMedico(Medico medico) {
        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(DatabaseHelper.LOGIN, medico.getLogin());
        valores.put(DatabaseHelper.NOME, medico.getNome());
        valores.put(DatabaseHelper.SENHA, medico.getSenha());

        resultado = db.insert(DatabaseHelper.TABELA_MEDICO, null, valores);
        db.close();

        return resultado;
    }

    public int loginMedico(String login, String senha) {
        Cursor cursor;
        String[] args =  {};
        db = banco.getReadableDatabase();
        String query = "SELECT "+ banco.ID +" FROM "+ banco.TABELA_MEDICO +" WHERE "+ banco.LOGIN +" = '"+ login +"' AND "+ banco.SENHA +" = '"+ senha + "' ORDER BY "+ banco.ID + " DESC LIMIT 1";
        cursor = db.rawQuery(query, args);

        int id;
        if(cursor!=null && cursor.moveToFirst()){
            id = cursor.getInt(0);
        } else {
            id = 0;
        }
        db.close();

        return id;
    }

    public String inserirEntrada(EntradaPaciente entradaPaciente) {
        Paciente paciente = entradaPaciente.getPaciente();
        String retorno = null;

        ContentValues valores;
        long resultado = -1;

        db = banco.getWritableDatabase();

        resultado = paciente.getId();
        if (resultado<1) {
            valores = new ContentValues();
            valores.put(DatabaseHelper.NOME, paciente.getNome());
            valores.put(DatabaseHelper.DESCRICAO, paciente.getDescricao());

            resultado = db.insert(DatabaseHelper.TABELA_PACIENTE, null, valores);
        }

        if (resultado !=-1) {
            valores = new ContentValues();
            valores.put(DatabaseHelper.PACIENTE_ID, resultado);
            valores.put(DatabaseHelper.MEDICO_ID, entradaPaciente.getMedico().getId());
            valores.put(DatabaseHelper.CHEGADA, entradaPaciente.getDataChegada());
            valores.put(DatabaseHelper.MEDICACAO, entradaPaciente.getMedicacao());
            valores.put(DatabaseHelper.DOENCA, entradaPaciente.getDoenca());
            valores.put(DatabaseHelper.CUSTO, String.valueOf(entradaPaciente.getCusto()));
            resultado = db.insert(DatabaseHelper.TABELA_ENTRADA, null, valores);
            db.close();
            if (resultado != -1) {
                retorno = "Paciente inserido com sucesso.";
            }
        }

        if (resultado == -1){
            db.close();
            retorno = "Erro ao inserir paciente";
        }

        return retorno;

    }

    public Cursor listarPacientes(long id, String nome, String data){
        Cursor cursor;
        String[] campos =  {banco.ID, banco.NOME};
        db = banco.getReadableDatabase();
        String query;
        String[] args =  {};
        if (nome!=null) {
            query = String.format("SELECT * FROM ENTRADA_PACIENTE e INNER JOIN PACIENTE p ON e.paciente_id = p._id WHERE p.nome = '%s' AND e.medico_id = '%d' group by e.paciente_id", nome, id);
        } else {
            query = String.format("SELECT * FROM ENTRADA_PACIENTE e INNER JOIN PACIENTE p ON e.paciente_id = p._id WHERE e.data_chegada = '%s' AND e.medico_id = '%d' group by e.paciente_id", data, id);
        }
        cursor = db.rawQuery(query,args);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
}