package com.ltp3.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String NOME_BANCO = "RELATORIO";
    public static final int VERSAO = 1;
    public static final String TABELA_PACIENTE = "PACIENTE";
    public static final String TABELA_MEDICO = "MEDICO";
    public static final String TABELA_ENTRADA = "ENTRADA_PACIENTE";
    public static final String ID = "_id";
    public static final String NOME = "nome";
    public static final String LOGIN = "login";
    public static final String SENHA = "senha";
    public static final String DESCRICAO = "descricao";
    public static final String MEDICO_ID = "medico_id";
    public static final String PACIENTE_ID = "paciente_id";
    public static final String CHEGADA = "data_chegada";
    public static final String MEDICACAO = "medicacao";
    public static final String DOENCA = "doenca";
    public static final String CUSTO = "custo";

    public DatabaseHelper(Context context){
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tabelaPaciente = "CREATE TABLE IF NOT EXISTS "+ TABELA_PACIENTE +" ("
                +ID+" integer primary key autoincrement,"
                +NOME+" text not null,"
                +DESCRICAO+" text"
                +")";

        String tabelaMedico = "CREATE TABLE IF NOT EXISTS  "+ TABELA_MEDICO +" ("
                +ID+" integer primary key autoincrement,"
                +NOME+" text not null,"
                +LOGIN+" text not null,"
                +SENHA+" text not null"
                +" )";

        String tabelaEntrada = "CREATE TABLE IF NOT EXISTS  "+ TABELA_ENTRADA +" ("
                +ID+" integer primary key autoincrement,"
                +PACIENTE_ID+" integer not null,"
                +MEDICO_ID+" integer not null,"
                +CHEGADA+" text not null,"
                +MEDICACAO+" text not null,"
                +DOENCA+" text not null,"
                +CUSTO+" decimal(10,2) not null,"
                + " FOREIGN KEY ( "+MEDICO_ID+" ) "
                + " REFERENCES " + TABELA_MEDICO + " ( "+ID+" ), "
                + " FOREIGN KEY ( "+PACIENTE_ID+" ) "
                + " REFERENCES " + TABELA_PACIENTE + " ( "+ID+" ) "
                +" )";

        db.execSQL(tabelaMedico);
        db.execSQL(tabelaPaciente);
        db.execSQL(tabelaEntrada);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA_ENTRADA);
        db.execSQL("DROP TABLE IF EXISTS" + TABELA_PACIENTE);
        db.execSQL("DROP TABLE IF EXISTS" + TABELA_MEDICO);
        onCreate(db);
    }
}